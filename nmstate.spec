%define srcname nmstate
%define libname libnmstate

Name:           nmstate
Version:        2.2.42
Release:        %autorelease
Summary:        Declarative network manager API
License:        Apache-2.0 AND LGPL-2.1-or-later
URL:            https://github.com/%{srcname}/%{srcname}
Source0:        %{url}/releases/download/v%{version}/%{srcname}-%{version}.tar.gz
Source1:        %{url}/releases/download/v%{version}/%{srcname}-%{version}.tar.gz.asc
Source2:        https://nmstate.io/nmstate.gpg
Source3:        %{url}/releases/download/v%{version}/%{srcname}-vendor-%{version}.tar.xz
Requires:       (nmstate-libs%{?_isa} = %{version}-%{release} if nmstate-libs)
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  gnupg2
BuildRequires:  systemd-devel
BuildRequires:  systemd-rpm-macros
BuildRequires:  rust-toolset

%description
Nmstate is a library with an accompanying command line tool that manages host
networking settings in a declarative manner and aimed to satisfy enterprise
needs to manage host networking through a northbound declarative API and multi
provider support on the southbound.


%package libs
Summary:        C binding of nmstate
# Use Recommends for NetworkManager because only access to NM DBus is required,
# but NM could be running on a different host
Recommends:     NetworkManager
# Avoid automatically generated profiles
Recommends:     NetworkManager-config-server
License:        Apache-2.0

%description libs
C binding of nmstate.

%package devel
Summary:        Development files for nmstate
Group:          Development/Libraries
Requires:       %{name}-libs%{?_isa} = %{version}-%{release}
License:        Apache-2.0

%description devel
Development files of nmstate C binding.

%package static
Summary:        Static development files for nmstate
Group:          Development/Libraries
License:        Apache-2.0
Requires:       %{name}-devel%{?_isa} = %{version}-%{release}

%description static
Static C library bindings for nmstate.

%package -n python3-%{libname}
Summary:        nmstate Python 3 API library
# Use Recommends for NetworkManager because only access to NM DBus is required,
# but NM could be running on a different host
Recommends:     NetworkManager
# Avoid automatically generated profiles
Recommends:     NetworkManager-config-server
Recommends:     (nmstate-plugin-ovsdb if openvswitch)
# Use Suggests for NetworkManager-ovs and NetworkManager-team since it is only
# required for OVS and team support
Suggests:       NetworkManager-ovs
Suggests:       NetworkManager-team
Provides:       nmstate-plugin-ovsdb = %{version}-%{release}
Requires:       %{name}-libs%{?_isa} = %{version}-%{release}
Obsoletes:      nmstate-plugin-ovsdb < 2.0-1
License:        Apache-2.0

%description -n python3-%{libname}
This package contains the Python 3 library for Nmstate.

%prep
gpg2 --import --import-options import-export,import-minimal \
    %{SOURCE2} > ./gpgkey-mantainers.gpg
gpgv2 --keyring ./gpgkey-mantainers.gpg %{SOURCE1} %{SOURCE0}

%autosetup -n %{name}-%{version_no_tilde} -p1 %{?rhel:-a3}

pushd rust
%if 0%{?rhel}
mv ../vendor ./
%cargo_prep -v vendor
%else
%cargo_prep
%endif
popd

%build
pushd rust
%cargo_build
%cargo_license_summary
%{cargo_license} > ../LICENSE.dependencies
%if 0%{?rhel}
%cargo_vendor_manifest
%endif
popd

pushd rust/src/python
%py3_build
popd

%install
env SKIP_PYTHON_INSTALL=1 \
    PREFIX=%{_prefix} \
    LIBDIR=%{_libdir} \
    SYSCONFDIR=%{_sysconfdir} \
    %make_install

pushd rust/src/python
%py3_install
popd

%files
%doc README.md
%license LICENSE.dependencies
%if 0%{?rhel}
%license rust/cargo-vendor.txt
%endif
%doc examples/
%{_mandir}/man8/nmstatectl.8*
%{_mandir}/man8/nmstate-autoconf.8*
%{_mandir}/man8/nmstate.service.8*
%{_bindir}/nmstatectl
%{_bindir}/nmstate-autoconf
%{_unitdir}/nmstate.service
%dir %{_sysconfdir}/%{name}
%{_sysconfdir}/%{name}/README

%files libs
%{_libdir}/libnmstate.so.*

%files devel
%{_libdir}/libnmstate.so
%{_includedir}/nmstate.h
%{_libdir}/pkgconfig/nmstate.pc

%files -n python3-%{libname}
%license LICENSE
%{python3_sitelib}/%{libname}
%{python3_sitelib}/%{srcname}-*.egg-info/

%files static
%{_libdir}/libnmstate.a

%changelog
%autochangelog
